﻿using CCOTest.Controllers.Base;
using Logic.Dto;
using Logic.Services;
using Microsoft.AspNetCore.Mvc;
using Models.Models;
using System;
using System.Threading.Tasks;

namespace CCOTest.Controllers
{
    public class ProductController : GenericActionController<Product, Guid>
    {
        private readonly IProductService _service;

        public ProductController(IProductService service) : base(service)
        {
            _service = service;
        }

        [HttpGet, Route("[action]")]
        public async Task<IActionResult> GetPaged([FromQuery] PagedParameters parameters)
            => Ok(await _service.GetPagedList(parameters));
    }
}
