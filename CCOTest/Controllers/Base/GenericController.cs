﻿using Logic.Services.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CCOTest.Controllers.Base
{
    [Route("api/[controller]")]
    public class GenericActionController<T, TKey> : ApiBaseController<T, TKey> where T : class, IModel
    {
        private readonly IService<T, TKey> _service;

        public GenericActionController(IService<T, TKey> service) : base(service)
            => _service = service;

        [HttpGet]
        public virtual IActionResult Get() => Ok(_service.GetAll());

        [HttpGet]
        [Route("{id}")]
        public virtual async Task<IActionResult> GetAsync(TKey id)
            => Ok(await _service.Find(id));

        [HttpPost]
        public virtual async Task<IActionResult> Post(T entity)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _service.Create(entity);
            return Ok(entity);
        }

        [HttpPut("{id}")]
        public virtual async Task<IActionResult> Put(TKey id, T entity)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _service.Modify(id, entity);
            return Ok(entity);
        }

        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> Delete(TKey id)
            => Ok(await _service.Delete(id));
    }
}
