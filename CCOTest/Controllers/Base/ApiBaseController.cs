﻿using Logic.Services.Base;
using Microsoft.AspNetCore.Mvc;
using Models.Generic;

namespace CCOTest.Controllers.Base
{
    [ApiController]
    public abstract class ApiBaseController<T, TKey> : ControllerBase
        where T : class, IModel
    {
        private static IService<T, TKey> _service;

        public ApiBaseController(IService<T, TKey> service)
        {
            _service = service;
        }

    }
}
