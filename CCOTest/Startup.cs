using Logic.Services;
using Logic.Services.Base;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Models.Context;
using Models.Extensions;
using Models.Models;
using Models.Uow;
using Newtonsoft.Json;
using Repositories.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CCOTest
{
    public class Startup
    {
        private string ConnectionString;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConnectionString = Configuration.GetConnectionString("AppDb");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AuthenticationSettings>(Configuration.GetSection("ApplicationSettings"));
            services.AddDbContext<AppDbContext>(item => item.UseSqlServer(Configuration.GetConnectionString("DbConnection")));
            services.AddControllers();

            services.RegisterConnection(ConnectionString);

            services.AddMvcCore()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddControllersAsServices()
                .AddNewtonsoftJson(o => o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore)
                .AddApiExplorer();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                    });
            });

            services.AddTokenAuthentication(Configuration);
            services.AddScoped<IUnitOfWork, UnitOfWork<AppDbContext>>();

            #region Repositories
            services.AddScoped<IGenericRepository<Product>, GenericRepository<Product>>();
            #endregion

            #region Services
            services.AddScoped<IProductService, ProductService>();
            //services.AddScoped<IGenericService<Product, Guid>, GenericService<Product, Guid>>();
            #endregion
        }

        #region Configure
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            InitializeDatabase(app);
            app.UseHttpsRedirection();
            app.UseCors("AllowAll");
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
        #endregion

        #region InitDatabase
        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<AppDbContext>().Database.Migrate();
            }
        }
        #endregion
    }
}
