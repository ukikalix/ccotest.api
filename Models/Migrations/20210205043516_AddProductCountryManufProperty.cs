﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class AddProductCountryManufProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("0893cbe6-60ea-46e9-8a57-5d0648420e4d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("329089d6-2dc7-4c41-b618-88ec332397c7"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("514ced80-787b-4e71-8420-6ccb6c139e48"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("68d4adae-b1af-4bf1-8af4-1fc4435a32e5"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6e58257f-e17a-400e-ad8b-95b80035e07d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("83a15874-a2b6-452f-b929-8bcaef9f6c22"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("8c020dc7-42ec-4fc4-bb5a-196d6feac70d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("92866a06-e24a-4a26-8b22-95151e4087a0"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a3afee47-197d-49c6-87b0-d8aa43d0d170"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a905a548-ff33-4c24-aa31-2ea26a0bd161"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("aec98e68-7783-4733-85b5-6d3e3cba253e"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b66a4b60-0ff0-46c0-84c6-77a218a8a090"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b790bffd-b51e-44f8-912f-6903b2da979c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ba575e78-2fd2-4537-a543-c0f28f60541e"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c49aac37-c352-4b00-96ed-8b61a40a1a9c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c5239db2-3b7b-4235-863c-d6f63933813b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("cf65c757-a5da-444e-a94f-50cfc02d4101"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("cfcb41c4-c4e5-4587-9119-6ae6141b18b7"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ea02032e-6995-49b4-a129-f4b8a44dac74"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("f1df0828-3581-4b55-bdca-058edc522cb4"));

            migrationBuilder.AddColumn<string>(
                name: "ManufacturerCountry",
                table: "Products",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Features", "LaunchedAt", "ManufacturerCountry", "ManufacturerEmail", "ManufacturerName", "Name", "Price", "Stock", "Thumbnail" },
                values: new object[,]
                {
                    { new Guid("6cadd258-40e3-4a1b-bb9f-04f041791ed9"), null, new DateTime(2021, 1, 26, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(3534), null, "TWELVESOUTH@yopmail.com", "TWELVE SOUTH", "Mac Inspired Candle", 29.99m, 100, "https://images-na.ssl-images-amazon.com/images/I/719IDvQolVL._AC_SL1500_.jpg" },
                    { new Guid("a890d405-21b2-4068-b217-954d265026ce"), null, new DateTime(2021, 2, 3, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5887), null, "MATNEY@yopmail.com", "MATNEY", "Kitty Thief Piggy Bank", 18.99m, 96, "https://images-na.ssl-images-amazon.com/images/I/61SzyUXACkL._AC_SL1200_.jpg" },
                    { new Guid("abe65b76-6a31-4829-a1ba-fdc252980d7f"), null, new DateTime(2021, 1, 30, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5885), null, "OSTRICHPILLOW@yopmail.com", "OSTRICH PILLOW", "Original Ostrich Pillow", 129.00m, 90, "https://images-na.ssl-images-amazon.com/images/I/61thviNNEhL._AC_SL1000_.jpg" },
                    { new Guid("8ef8d127-86f8-476d-8278-3404c1481c45"), null, new DateTime(2021, 1, 15, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5882), null, "NUNI@yopmail.com", "NUNI", "Tortilla Toaster", 12.89m, 45, "https://images-na.ssl-images-amazon.com/images/I/817qe69ttmL._AC_SL1500_.jpg" },
                    { new Guid("40dd63a7-9b55-4edb-a104-c53cddbcf99a"), null, new DateTime(2020, 11, 30, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5878), null, "GORGEROUS@yopmail.com", "GORGEROUS", "Thanos One-Piece Swimsuit for Men and Boys", 35.99m, 30, "https://images-na.ssl-images-amazon.com/images/I/61jFyhk5WfL._AC_UX522_.jpg" },
                    { new Guid("d8183080-27a9-45bf-a35c-6a4d0b59ef96"), null, new DateTime(2021, 2, 3, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5875), null, "YESITO@yopmail.com", "YESITO", "Chicken Harness and Leash", 15.99m, 34, "https://images-na.ssl-images-amazon.com/images/I/71Q26wBEIeL._AC_SL1500_.jpg" },
                    { new Guid("8d615707-a754-4614-a440-c1fee5e718f2"), null, new DateTime(2021, 1, 13, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5873), null, "AROS@yopmail.com", "AROS", "Inflatable Zip Up Hoodie", 69.00m, 90, "https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1565356340-aros-standard-inflatable-hoodie-1565356312.png?crop=0.761xw:1.00xh;0.0712xw,0&resize=480:*" },
                    { new Guid("027eb11f-b8ab-470b-994a-1658b49b34f4"), null, new DateTime(2020, 12, 23, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5871), null, "SANIDIKA@yopmail.com", "SANIDIKA", "Mobile Phone Jail Cell", 12.89m, 123, "https://images-na.ssl-images-amazon.com/images/I/511KgRo6IML._AC_.jpg" },
                    { new Guid("56e172c9-3a85-412d-a9eb-b68fad1872a2"), null, new DateTime(2021, 1, 18, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5869), null, "LAUYOO@yopmail.com", "LAUYOO", "Frog Eye Mask", 5.00m, 22, "https://images-na.ssl-images-amazon.com/images/I/61V%2Bd63uzhL._AC_UX679_.jpg" },
                    { new Guid("3c571f44-04a5-49bd-9c87-9a434171eec6"), null, new DateTime(2021, 1, 22, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5866), null, "SWIMWAYS@yopmail.com", "SWIMWAYS", "Inflatable Sloth Float", 31.10m, 90, "https://images-na.ssl-images-amazon.com/images/I/418IDuDQouL._AC_.jpg" },
                    { new Guid("c681b70f-21d8-43fb-b394-aed35c9a215c"), null, new DateTime(2020, 11, 21, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5863), null, "BRUNSWICK@yopmail.com", "BRUNSWICK", "Lizard Glow Bowling Ball", 99.95m, 3, "https://images-na.ssl-images-amazon.com/images/I/71EVlmgnhCL._AC_SL1200_.jpg" },
                    { new Guid("1ee3cb08-0d83-4c41-a01c-193e6f3146df"), null, new DateTime(2021, 1, 22, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5861), null, "CHARCOALCOMPANION@yopmail.com", "CHARCOAL COMPANION", "Meat Shredder Claws", 23.12m, 23, "https://images-na.ssl-images-amazon.com/images/I/71Z0h08fPtL._AC_SL1500_.jpg" },
                    { new Guid("19f6adac-4510-4468-be15-1d5f1ebd5a50"), null, new DateTime(2021, 1, 22, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5847), null, "JETCREATIONS@yopmail.com", "JET CREATIONS", "Lifelike Elephant Inflatable", 12.12m, 90, "https://images-na.ssl-images-amazon.com/images/I/81N0OA0JNgL._AC_SL1500_.jpg" },
                    { new Guid("3eee92a4-b780-4bbb-8aa0-2db46267f8be"), null, new DateTime(2021, 1, 13, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5845), null, "CHIA@yopmail.com", "CHIA", "Chia Pet Gremlin", 19.99m, 12, "https://images-na.ssl-images-amazon.com/images/I/81NOcM2rTDL._AC_SL1500_.jpg" },
                    { new Guid("a259de55-92c9-426b-b096-a1e76bf01698"), null, new DateTime(2021, 1, 20, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5842), null, "PENGUINRANDOMHOUSE@yopmail.com", "PENGUIN RANDOM HOUSE LLC", "The Official 'A Game of Thrones' Coloring Book", 11.86m, 43, "https://images-na.ssl-images-amazon.com/images/I/61YbguJp1jL._SX258_BO1,204,203,200_.jpg" },
                    { new Guid("02ed8249-a478-41fb-9234-0b573272a376"), null, new DateTime(2021, 1, 24, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5840), null, "PREPWORKSFROMPROGRESSIVE@yopmail.com", "PREPWORKS FROM PROGRESSIVE", "Guacamole Bowl", 9.99m, 89, "https://images-na.ssl-images-amazon.com/images/I/81Gbsf53NWL._AC_SL1500_.jpg" },
                    { new Guid("21a67c6a-3b96-4bc8-bced-4d12e1fd2bab"), null, new DateTime(2021, 1, 24, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5834), null, "COMFYCUP@yopmail.com", "COMFYCUP", "Comfycup Public Transportation Cup Holder", 16.99m, 68, "https://images-na.ssl-images-amazon.com/images/I/61hK4dniSiL._AC_SL1500_.jpg" },
                    { new Guid("50f7de8c-5330-4154-a4ae-0da41dfe5114"), null, new DateTime(2021, 1, 24, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5387), null, "WILTON@yopmail.com", "WILTON", "Wilton Edible Glitter", 6.29m, 74, "https://images-na.ssl-images-amazon.com/images/I/81qm-uqROaL._AC_SL1500_.jpg" },
                    { new Guid("f1e93b9d-43c4-465e-8215-87c493c9eee4"), null, new DateTime(2021, 1, 31, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5889), null, "SARUTGROUP@yopmail.com", "THE SARUT GROUP", "The Hen Bag Handbag", 42.99m, 36, "https://images-na.ssl-images-amazon.com/images/I/61eiIozbjeL._AC_SL1200_.jpg" },
                    { new Guid("0165b92e-29ef-41b1-9026-24a7d05c6b87"), null, new DateTime(2021, 1, 27, 4, 35, 16, 618, DateTimeKind.Utc).AddTicks(5891), null, "THENICE@yopmail.com", "THENICE", "Anatomy Bathing Suit", 16.99m, 76, "https://images-na.ssl-images-amazon.com/images/I/518cgLwb27L._AC_UY550_.jpg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("0165b92e-29ef-41b1-9026-24a7d05c6b87"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("027eb11f-b8ab-470b-994a-1658b49b34f4"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("02ed8249-a478-41fb-9234-0b573272a376"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("19f6adac-4510-4468-be15-1d5f1ebd5a50"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("1ee3cb08-0d83-4c41-a01c-193e6f3146df"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("21a67c6a-3b96-4bc8-bced-4d12e1fd2bab"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("3c571f44-04a5-49bd-9c87-9a434171eec6"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("3eee92a4-b780-4bbb-8aa0-2db46267f8be"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("40dd63a7-9b55-4edb-a104-c53cddbcf99a"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("50f7de8c-5330-4154-a4ae-0da41dfe5114"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("56e172c9-3a85-412d-a9eb-b68fad1872a2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6cadd258-40e3-4a1b-bb9f-04f041791ed9"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("8d615707-a754-4614-a440-c1fee5e718f2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("8ef8d127-86f8-476d-8278-3404c1481c45"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a259de55-92c9-426b-b096-a1e76bf01698"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a890d405-21b2-4068-b217-954d265026ce"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("abe65b76-6a31-4829-a1ba-fdc252980d7f"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c681b70f-21d8-43fb-b394-aed35c9a215c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("d8183080-27a9-45bf-a35c-6a4d0b59ef96"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("f1e93b9d-43c4-465e-8215-87c493c9eee4"));

            migrationBuilder.DropColumn(
                name: "ManufacturerCountry",
                table: "Products");

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Features", "LaunchedAt", "ManufacturerEmail", "ManufacturerName", "Name", "Price", "Stock", "Thumbnail" },
                values: new object[,]
                {
                    { new Guid("329089d6-2dc7-4c41-b618-88ec332397c7"), null, new DateTime(2021, 1, 25, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(1974), "TWELVESOUTH@yopmail.com", "TWELVE SOUTH", "Mac Inspired Candle", 29.99m, 100, "https://images-na.ssl-images-amazon.com/images/I/719IDvQolVL._AC_SL1500_.jpg" },
                    { new Guid("aec98e68-7783-4733-85b5-6d3e3cba253e"), null, new DateTime(2021, 2, 2, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3931), "MATNEY@yopmail.com", "MATNEY", "Kitty Thief Piggy Bank", 18.99m, 96, "https://images-na.ssl-images-amazon.com/images/I/61SzyUXACkL._AC_SL1200_.jpg" },
                    { new Guid("ba575e78-2fd2-4537-a543-c0f28f60541e"), null, new DateTime(2021, 1, 29, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3930), "OSTRICHPILLOW@yopmail.com", "OSTRICH PILLOW", "Original Ostrich Pillow", 129.00m, 90, "https://images-na.ssl-images-amazon.com/images/I/61thviNNEhL._AC_SL1000_.jpg" },
                    { new Guid("c5239db2-3b7b-4235-863c-d6f63933813b"), null, new DateTime(2021, 1, 14, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3925), "NUNI@yopmail.com", "NUNI", "Tortilla Toaster", 12.89m, 45, "https://images-na.ssl-images-amazon.com/images/I/817qe69ttmL._AC_SL1500_.jpg" },
                    { new Guid("68d4adae-b1af-4bf1-8af4-1fc4435a32e5"), null, new DateTime(2020, 11, 29, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3923), "GORGEROUS@yopmail.com", "GORGEROUS", "Thanos One-Piece Swimsuit for Men and Boys", 35.99m, 30, "https://images-na.ssl-images-amazon.com/images/I/61jFyhk5WfL._AC_UX522_.jpg" },
                    { new Guid("b66a4b60-0ff0-46c0-84c6-77a218a8a090"), null, new DateTime(2021, 2, 2, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3921), "YESITO@yopmail.com", "YESITO", "Chicken Harness and Leash", 15.99m, 34, "https://images-na.ssl-images-amazon.com/images/I/71Q26wBEIeL._AC_SL1500_.jpg" },
                    { new Guid("ea02032e-6995-49b4-a129-f4b8a44dac74"), null, new DateTime(2021, 1, 12, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3920), "AROS@yopmail.com", "AROS", "Inflatable Zip Up Hoodie", 69.00m, 90, "https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1565356340-aros-standard-inflatable-hoodie-1565356312.png?crop=0.761xw:1.00xh;0.0712xw,0&resize=480:*" },
                    { new Guid("0893cbe6-60ea-46e9-8a57-5d0648420e4d"), null, new DateTime(2020, 12, 22, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3918), "SANIDIKA@yopmail.com", "SANIDIKA", "Mobile Phone Jail Cell", 12.89m, 123, "https://images-na.ssl-images-amazon.com/images/I/511KgRo6IML._AC_.jpg" },
                    { new Guid("cf65c757-a5da-444e-a94f-50cfc02d4101"), null, new DateTime(2021, 1, 17, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3917), "LAUYOO@yopmail.com", "LAUYOO", "Frog Eye Mask", 5.00m, 22, "https://images-na.ssl-images-amazon.com/images/I/61V%2Bd63uzhL._AC_UX679_.jpg" },
                    { new Guid("514ced80-787b-4e71-8420-6ccb6c139e48"), null, new DateTime(2021, 1, 21, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3915), "SWIMWAYS@yopmail.com", "SWIMWAYS", "Inflatable Sloth Float", 31.10m, 90, "https://images-na.ssl-images-amazon.com/images/I/418IDuDQouL._AC_.jpg" },
                    { new Guid("b790bffd-b51e-44f8-912f-6903b2da979c"), null, new DateTime(2020, 11, 20, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3912), "BRUNSWICK@yopmail.com", "BRUNSWICK", "Lizard Glow Bowling Ball", 99.95m, 3, "https://images-na.ssl-images-amazon.com/images/I/71EVlmgnhCL._AC_SL1200_.jpg" },
                    { new Guid("c49aac37-c352-4b00-96ed-8b61a40a1a9c"), null, new DateTime(2021, 1, 21, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3905), "CHARCOALCOMPANION@yopmail.com", "CHARCOAL COMPANION", "Meat Shredder Claws", 23.12m, 23, "https://images-na.ssl-images-amazon.com/images/I/71Z0h08fPtL._AC_SL1500_.jpg" },
                    { new Guid("cfcb41c4-c4e5-4587-9119-6ae6141b18b7"), null, new DateTime(2021, 1, 21, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3903), "JETCREATIONS@yopmail.com", "JET CREATIONS", "Lifelike Elephant Inflatable", 12.12m, 90, "https://images-na.ssl-images-amazon.com/images/I/81N0OA0JNgL._AC_SL1500_.jpg" },
                    { new Guid("8c020dc7-42ec-4fc4-bb5a-196d6feac70d"), null, new DateTime(2021, 1, 12, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3900), "CHIA@yopmail.com", "CHIA", "Chia Pet Gremlin", 19.99m, 12, "https://images-na.ssl-images-amazon.com/images/I/81NOcM2rTDL._AC_SL1500_.jpg" },
                    { new Guid("83a15874-a2b6-452f-b929-8bcaef9f6c22"), null, new DateTime(2021, 1, 19, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3898), "PENGUINRANDOMHOUSE@yopmail.com", "PENGUIN RANDOM HOUSE LLC", "The Official 'A Game of Thrones' Coloring Book", 11.86m, 43, "https://images-na.ssl-images-amazon.com/images/I/61YbguJp1jL._SX258_BO1,204,203,200_.jpg" },
                    { new Guid("6e58257f-e17a-400e-ad8b-95b80035e07d"), null, new DateTime(2021, 1, 23, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3896), "PREPWORKSFROMPROGRESSIVE@yopmail.com", "PREPWORKS FROM PROGRESSIVE", "Guacamole Bowl", 9.99m, 89, "https://images-na.ssl-images-amazon.com/images/I/81Gbsf53NWL._AC_SL1500_.jpg" },
                    { new Guid("92866a06-e24a-4a26-8b22-95151e4087a0"), null, new DateTime(2021, 1, 23, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3894), "COMFYCUP@yopmail.com", "COMFYCUP", "Comfycup Public Transportation Cup Holder", 16.99m, 68, "https://images-na.ssl-images-amazon.com/images/I/61hK4dniSiL._AC_SL1500_.jpg" },
                    { new Guid("a3afee47-197d-49c6-87b0-d8aa43d0d170"), null, new DateTime(2021, 1, 23, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3838), "WILTON@yopmail.com", "WILTON", "Wilton Edible Glitter", 6.29m, 74, "https://images-na.ssl-images-amazon.com/images/I/81qm-uqROaL._AC_SL1500_.jpg" },
                    { new Guid("f1df0828-3581-4b55-bdca-058edc522cb4"), null, new DateTime(2021, 1, 30, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3933), "SARUTGROUP@yopmail.com", "THE SARUT GROUP", "The Hen Bag Handbag", 42.99m, 36, "https://images-na.ssl-images-amazon.com/images/I/61eiIozbjeL._AC_SL1200_.jpg" },
                    { new Guid("a905a548-ff33-4c24-aa31-2ea26a0bd161"), null, new DateTime(2021, 1, 26, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3935), "THENICE@yopmail.com", "THENICE", "Anatomy Bathing Suit", 16.99m, 76, "https://images-na.ssl-images-amazon.com/images/I/518cgLwb27L._AC_UY550_.jpg" }
                });
        }
    }
}
