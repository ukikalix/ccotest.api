﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Features = table.Column<string>(nullable: true),
                    LaunchedAt = table.Column<DateTime>(nullable: false),
                    ManufacturerEmail = table.Column<string>(nullable: true),
                    ManufacturerName = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    Stock = table.Column<int>(nullable: false),
                    Thumbnail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Features", "LaunchedAt", "ManufacturerEmail", "ManufacturerName", "Name", "Price", "Stock", "Thumbnail" },
                values: new object[,]
                {
                    { new Guid("329089d6-2dc7-4c41-b618-88ec332397c7"), null, new DateTime(2021, 1, 25, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(1974), "TWELVESOUTH@yopmail.com", "TWELVE SOUTH", "Mac Inspired Candle", 29.99m, 100, "https://images-na.ssl-images-amazon.com/images/I/719IDvQolVL._AC_SL1500_.jpg" },
                    { new Guid("aec98e68-7783-4733-85b5-6d3e3cba253e"), null, new DateTime(2021, 2, 2, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3931), "MATNEY@yopmail.com", "MATNEY", "Kitty Thief Piggy Bank", 18.99m, 96, "https://images-na.ssl-images-amazon.com/images/I/61SzyUXACkL._AC_SL1200_.jpg" },
                    { new Guid("ba575e78-2fd2-4537-a543-c0f28f60541e"), null, new DateTime(2021, 1, 29, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3930), "OSTRICHPILLOW@yopmail.com", "OSTRICH PILLOW", "Original Ostrich Pillow", 129.00m, 90, "https://images-na.ssl-images-amazon.com/images/I/61thviNNEhL._AC_SL1000_.jpg" },
                    { new Guid("c5239db2-3b7b-4235-863c-d6f63933813b"), null, new DateTime(2021, 1, 14, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3925), "NUNI@yopmail.com", "NUNI", "Tortilla Toaster", 12.89m, 45, "https://images-na.ssl-images-amazon.com/images/I/817qe69ttmL._AC_SL1500_.jpg" },
                    { new Guid("68d4adae-b1af-4bf1-8af4-1fc4435a32e5"), null, new DateTime(2020, 11, 29, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3923), "GORGEROUS@yopmail.com", "GORGEROUS", "Thanos One-Piece Swimsuit for Men and Boys", 35.99m, 30, "https://images-na.ssl-images-amazon.com/images/I/61jFyhk5WfL._AC_UX522_.jpg" },
                    { new Guid("b66a4b60-0ff0-46c0-84c6-77a218a8a090"), null, new DateTime(2021, 2, 2, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3921), "YESITO@yopmail.com", "YESITO", "Chicken Harness and Leash", 15.99m, 34, "https://images-na.ssl-images-amazon.com/images/I/71Q26wBEIeL._AC_SL1500_.jpg" },
                    { new Guid("ea02032e-6995-49b4-a129-f4b8a44dac74"), null, new DateTime(2021, 1, 12, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3920), "AROS@yopmail.com", "AROS", "Inflatable Zip Up Hoodie", 69.00m, 90, "https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1565356340-aros-standard-inflatable-hoodie-1565356312.png?crop=0.761xw:1.00xh;0.0712xw,0&resize=480:*" },
                    { new Guid("0893cbe6-60ea-46e9-8a57-5d0648420e4d"), null, new DateTime(2020, 12, 22, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3918), "SANIDIKA@yopmail.com", "SANIDIKA", "Mobile Phone Jail Cell", 12.89m, 123, "https://images-na.ssl-images-amazon.com/images/I/511KgRo6IML._AC_.jpg" },
                    { new Guid("cf65c757-a5da-444e-a94f-50cfc02d4101"), null, new DateTime(2021, 1, 17, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3917), "LAUYOO@yopmail.com", "LAUYOO", "Frog Eye Mask", 5.00m, 22, "https://images-na.ssl-images-amazon.com/images/I/61V%2Bd63uzhL._AC_UX679_.jpg" },
                    { new Guid("514ced80-787b-4e71-8420-6ccb6c139e48"), null, new DateTime(2021, 1, 21, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3915), "SWIMWAYS@yopmail.com", "SWIMWAYS", "Inflatable Sloth Float", 31.10m, 90, "https://images-na.ssl-images-amazon.com/images/I/418IDuDQouL._AC_.jpg" },
                    { new Guid("b790bffd-b51e-44f8-912f-6903b2da979c"), null, new DateTime(2020, 11, 20, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3912), "BRUNSWICK@yopmail.com", "BRUNSWICK", "Lizard Glow Bowling Ball", 99.95m, 3, "https://images-na.ssl-images-amazon.com/images/I/71EVlmgnhCL._AC_SL1200_.jpg" },
                    { new Guid("c49aac37-c352-4b00-96ed-8b61a40a1a9c"), null, new DateTime(2021, 1, 21, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3905), "CHARCOALCOMPANION@yopmail.com", "CHARCOAL COMPANION", "Meat Shredder Claws", 23.12m, 23, "https://images-na.ssl-images-amazon.com/images/I/71Z0h08fPtL._AC_SL1500_.jpg" },
                    { new Guid("cfcb41c4-c4e5-4587-9119-6ae6141b18b7"), null, new DateTime(2021, 1, 21, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3903), "JETCREATIONS@yopmail.com", "JET CREATIONS", "Lifelike Elephant Inflatable", 12.12m, 90, "https://images-na.ssl-images-amazon.com/images/I/81N0OA0JNgL._AC_SL1500_.jpg" },
                    { new Guid("8c020dc7-42ec-4fc4-bb5a-196d6feac70d"), null, new DateTime(2021, 1, 12, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3900), "CHIA@yopmail.com", "CHIA", "Chia Pet Gremlin", 19.99m, 12, "https://images-na.ssl-images-amazon.com/images/I/81NOcM2rTDL._AC_SL1500_.jpg" },
                    { new Guid("83a15874-a2b6-452f-b929-8bcaef9f6c22"), null, new DateTime(2021, 1, 19, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3898), "PENGUINRANDOMHOUSE@yopmail.com", "PENGUIN RANDOM HOUSE LLC", "The Official 'A Game of Thrones' Coloring Book", 11.86m, 43, "https://images-na.ssl-images-amazon.com/images/I/61YbguJp1jL._SX258_BO1,204,203,200_.jpg" },
                    { new Guid("6e58257f-e17a-400e-ad8b-95b80035e07d"), null, new DateTime(2021, 1, 23, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3896), "PREPWORKSFROMPROGRESSIVE@yopmail.com", "PREPWORKS FROM PROGRESSIVE", "Guacamole Bowl", 9.99m, 89, "https://images-na.ssl-images-amazon.com/images/I/81Gbsf53NWL._AC_SL1500_.jpg" },
                    { new Guid("92866a06-e24a-4a26-8b22-95151e4087a0"), null, new DateTime(2021, 1, 23, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3894), "COMFYCUP@yopmail.com", "COMFYCUP", "Comfycup Public Transportation Cup Holder", 16.99m, 68, "https://images-na.ssl-images-amazon.com/images/I/61hK4dniSiL._AC_SL1500_.jpg" },
                    { new Guid("a3afee47-197d-49c6-87b0-d8aa43d0d170"), null, new DateTime(2021, 1, 23, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3838), "WILTON@yopmail.com", "WILTON", "Wilton Edible Glitter", 6.29m, 74, "https://images-na.ssl-images-amazon.com/images/I/81qm-uqROaL._AC_SL1500_.jpg" },
                    { new Guid("f1df0828-3581-4b55-bdca-058edc522cb4"), null, new DateTime(2021, 1, 30, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3933), "SARUTGROUP@yopmail.com", "THE SARUT GROUP", "The Hen Bag Handbag", 42.99m, 36, "https://images-na.ssl-images-amazon.com/images/I/61eiIozbjeL._AC_SL1200_.jpg" },
                    { new Guid("a905a548-ff33-4c24-aa31-2ea26a0bd161"), null, new DateTime(2021, 1, 26, 22, 57, 36, 541, DateTimeKind.Utc).AddTicks(3935), "THENICE@yopmail.com", "THENICE", "Anatomy Bathing Suit", 16.99m, 76, "https://images-na.ssl-images-amazon.com/images/I/518cgLwb27L._AC_UY550_.jpg" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
