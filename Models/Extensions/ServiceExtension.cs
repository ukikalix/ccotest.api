﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Models.Factories;

namespace Models.Extensions
{
    public static class ServiceExtension
    {
        public static void RegisterConnection(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IDbFactory>(e => new SqlFactory(connectionString));
        }
    }
}
