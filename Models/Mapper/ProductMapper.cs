﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.Interfaces;
using Models.Models;
using System;

namespace Models.Mapper
{
    public class ProductMapper : IEntityMappingConfiguration<Product>
    {
        /// <summary>
        /// Car entity type configuration
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products").HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.Price).HasColumnType("decimal(15,2)").IsRequired();

            builder.HasData(
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Mac Inspired Candle",
                    Price = 29.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-10),
                    ManufacturerEmail = "TWELVESOUTH@yopmail.com",
                    ManufacturerName = "TWELVE SOUTH",
                    Stock = 100,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/719IDvQolVL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Wilton Edible Glitter",
                    Price = 6.29M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-12),
                    ManufacturerEmail = "WILTON@yopmail.com",
                    ManufacturerName = "WILTON",
                    Stock = 74,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/81qm-uqROaL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Comfycup Public Transportation Cup Holder",
                    Price = 16.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-12),
                    ManufacturerEmail = "COMFYCUP@yopmail.com",
                    ManufacturerName = "COMFYCUP",
                    Stock = 68,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61hK4dniSiL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Guacamole Bowl",
                    Price = 9.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-12),
                    ManufacturerEmail = "PREPWORKSFROMPROGRESSIVE@yopmail.com",
                    ManufacturerName = "PREPWORKS FROM PROGRESSIVE",
                    Stock = 89,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/81Gbsf53NWL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "The Official 'A Game of Thrones' Coloring Book",
                    Price = 11.86M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-16),
                    ManufacturerEmail = "PENGUINRANDOMHOUSE@yopmail.com",
                    ManufacturerName = "PENGUIN RANDOM HOUSE LLC",
                    Stock = 43,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61YbguJp1jL._SX258_BO1,204,203,200_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Chia Pet Gremlin",
                    Price = 19.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-23),
                    ManufacturerEmail = "CHIA@yopmail.com",
                    ManufacturerName = "CHIA",
                    Stock = 12,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/81NOcM2rTDL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Lifelike Elephant Inflatable",
                    Price = 12.12M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-14),
                    ManufacturerEmail = "JETCREATIONS@yopmail.com",
                    ManufacturerName = "JET CREATIONS",
                    Stock = 90,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/81N0OA0JNgL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Meat Shredder Claws",
                    Price = 23.12M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-14),
                    ManufacturerEmail = "CHARCOALCOMPANION@yopmail.com",
                    ManufacturerName = "CHARCOAL COMPANION",
                    Stock = 23,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/71Z0h08fPtL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Lizard Glow Bowling Ball",
                    Price = 99.95M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-76),
                    ManufacturerEmail = "BRUNSWICK@yopmail.com",
                    ManufacturerName = "BRUNSWICK",
                    Stock = 3,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/71EVlmgnhCL._AC_SL1200_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Inflatable Sloth Float",
                    Price = 31.10M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-14),
                    ManufacturerEmail = "SWIMWAYS@yopmail.com",
                    ManufacturerName = "SWIMWAYS",
                    Stock = 90,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/418IDuDQouL._AC_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Frog Eye Mask",
                    Price = 5.00M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-18),
                    ManufacturerEmail = "LAUYOO@yopmail.com",
                    ManufacturerName = "LAUYOO",
                    Stock = 22,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61V%2Bd63uzhL._AC_UX679_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Mobile Phone Jail Cell",
                    Price = 12.89M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-44),
                    ManufacturerEmail = "SANIDIKA@yopmail.com",
                    ManufacturerName = "SANIDIKA",
                    Stock = 123,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/511KgRo6IML._AC_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Inflatable Zip Up Hoodie",
                    Price = 69.00M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-23),
                    ManufacturerEmail = "AROS@yopmail.com",
                    ManufacturerName = "AROS",
                    Stock = 90,
                    Thumbnail = "https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1565356340-aros-standard-inflatable-hoodie-1565356312.png?crop=0.761xw:1.00xh;0.0712xw,0&resize=480:*"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Chicken Harness and Leash",
                    Price = 15.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-2),
                    ManufacturerEmail = "YESITO@yopmail.com",
                    ManufacturerName = "YESITO",
                    Stock = 34,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/71Q26wBEIeL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Thanos One-Piece Swimsuit for Men and Boys",
                    Price = 35.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-67),
                    ManufacturerEmail = "GORGEROUS@yopmail.com",
                    ManufacturerName = "GORGEROUS",
                    Stock = 30,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61jFyhk5WfL._AC_UX522_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Tortilla Toaster",
                    Price = 12.89M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-21),
                    ManufacturerEmail = "NUNI@yopmail.com",
                    ManufacturerName = "NUNI",
                    Stock = 45,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/817qe69ttmL._AC_SL1500_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Original Ostrich Pillow",
                    Price = 129.00M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-6),
                    ManufacturerEmail = "OSTRICHPILLOW@yopmail.com",
                    ManufacturerName = "OSTRICH PILLOW",
                    Stock = 90,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61thviNNEhL._AC_SL1000_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Kitty Thief Piggy Bank",
                    Price = 18.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-2),
                    ManufacturerEmail = "MATNEY@yopmail.com",
                    ManufacturerName = "MATNEY",
                    Stock = 96,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61SzyUXACkL._AC_SL1200_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "The Hen Bag Handbag",
                    Price = 42.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-5),
                    ManufacturerEmail = "SARUTGROUP@yopmail.com",
                    ManufacturerName = "THE SARUT GROUP",
                    Stock = 36,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61eiIozbjeL._AC_SL1200_.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Anatomy Bathing Suit",
                    Price = 16.99M,
                    LaunchedAt = DateTime.UtcNow.AddDays(-9),
                    ManufacturerEmail = "THENICE@yopmail.com",
                    ManufacturerName = "THENICE",
                    Stock = 76,
                    Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/518cgLwb27L._AC_UY550_.jpg"
                }
           );
        }
    }
}
