﻿using Models.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Models
{
    public abstract class Catalog<T> : ICatalog<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
    }

    public interface ICatalog<TKey> : IModel<TKey>
    {
        string Name { get; set; }
    }
}
