﻿using System;

namespace Models.Models
{
    public class Product : Catalog<Guid>
    {
        public string Features { get; set; }
        public DateTime LaunchedAt { get; set; }
        public string ManufacturerEmail { get; set; }
        public string ManufacturerName { get; set; }
        public string ManufacturerCountry { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public string Thumbnail { get; set; }

    }
}
