﻿using Microsoft.EntityFrameworkCore;

namespace Models.Interfaces
{
    public interface IEntityMappingConfiguration<T> : IEntityTypeConfiguration<T> where T : class
    {
    }
}
