﻿using Logic.Dto;
using Logic.Services.Base;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public interface IProductService : IService<Product, Guid>
    {
        Task<PagedList<Product>> GetPagedList(PagedParameters parameters);
    }
}
