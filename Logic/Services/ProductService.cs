﻿using Logic.Dto;
using Logic.Services.Base;
using Models.Models;
using Models.Uow;
using Repositories.Repositories.Base;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Logic.Extensions;
using Logic.Filters;

namespace Logic.Services
{
    public class ProductService : Service<Product, Guid>, IProductService
    {
        private readonly IGenericRepository<Product> _repository;
        private readonly IUnitOfWork _uow;

        public ProductService(IUnitOfWork uow,
            IGenericRepository<Product> repository) : base(uow, repository)
        {
            _repository = repository;
            _uow = uow;
        }

        public async Task<PagedList<Product>> GetPagedList(PagedParameters parameters)
        {
            var orderType = parameters.Ascending ? "ASC" : "DESC";
            var orderProperty = string.IsNullOrEmpty(parameters.OrderProperty) ? "Id" : parameters.OrderProperty;
            var searchFilter = !string.IsNullOrEmpty(parameters.Search) ? ProductFilter.BySearchValue(parameters.Search) : ProductFilter.Default();

            var query = _repository.FindBy(searchFilter);

            return await query.OrderBy($"{orderProperty} {orderType}").AsQueryable().GetPagedResultAsync(parameters.CurrentPage, parameters.PageSize);
        }
    }
}
