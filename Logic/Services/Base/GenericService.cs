﻿using Models.Generic;
using Models.Uow;
using Repositories.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Services.Base
{
    public class GenericService<TEntity, TKey> : Service<TEntity, TKey>, IGenericService<TEntity, TKey>
        where TEntity : class, IModel<TKey>
    {
        public GenericService(IUnitOfWork uow, IGenericRepository<TEntity> repository) : base(uow, repository)
        {
        }
    }
}
