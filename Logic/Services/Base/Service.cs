﻿using Models.Generic;
using Models.Uow;
using Repositories.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services.Base
{
    public abstract class Service<TEntity, TKey> : IService<TEntity, TKey>
        where TEntity : class, IModel<TKey>, IModel
    {
        private readonly IBaseRepository<TEntity> _repository;
        private readonly IUnitOfWork _uow;

        public Service(IUnitOfWork uow,
            IBaseRepository<TEntity> repository)
        {
            _uow = uow;
            _repository = repository;
        }

        /// <summary>
        /// Get all rows from entity
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAll() => _repository.GetAll();

        /// <summary>
        /// Create entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual async Task<TEntity> Create(TEntity entity)
        {
            var record = _repository.Add(entity);
            await _uow.CommitAsync();
            return record;
        }

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual async Task Modify(TKey id, TEntity entity)
        {
            if (!await ValidateEntityOnUpdate(id, entity))
                return;
            _repository.Edit(entity);
            await _uow.CommitAsync();
        }

        /// <summary>
        /// Hard delete entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<TEntity> Delete(TKey id)
        {
            var entity = await _repository.FindByKey(id);
            if (entity == null)
                return null;

            _repository.Delete(entity);
            await _uow.CommitAsync();
            return entity;
        }

        /// <summary>
        /// Find entities by keys
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public virtual async Task<TEntity> Find(params object[] keyValues)
        {
            var record = await _repository.FindByKey(keyValues);
            if (record == null)
                return null;
            return record;
        }

        /// <summary>
        /// FInd entity by expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> expression)
        => _repository.FindBy(expression);

        /// <summary>
        /// Validate entity before update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        private async Task<bool> ValidateEntityOnUpdate(TKey id, TEntity entity)
        {
            var entityId = (entity as IModel<TKey>).Id;

            if (!Equals(entityId, id))
                return false;

            Expression<Func<TEntity, bool>> expression = x => x.Id.Equals(id);

            var record = await _repository.Any(expression);
            if (!record)
                return false;
            return true;
        }
    }
}
