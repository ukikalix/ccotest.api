﻿using Logic.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Extensions
{
    public static class IQueryableExtensions
    {
        public async static Task<PagedList<T>> GetPagedResultAsync<T>(this IQueryable<T> query, int currentPage, int pageSize) where T : class
        {
            int rowCount = await Task.Run(() => query.Count());
            List<T> results = await Task.Run(() => query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList());

            return new PagedList<T>
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                RowCount = rowCount,
                Results = results,
            };
        }
    }
}
