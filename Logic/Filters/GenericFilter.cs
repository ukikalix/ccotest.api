﻿using System;
using System.Linq.Expressions;

namespace Logic.Filters
{
    abstract class GenericFilter<T>
    {
        protected internal static Expression<Func<T, bool>> Default() => p => true;
    }
}
