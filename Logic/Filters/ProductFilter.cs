﻿using Models.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Logic.Filters
{
    class ProductFilter : GenericFilter<Product>
    {
        protected internal static Expression<Func<Product, bool>> BySearchValue(string value)
        {
            string[] values = value.ToLower().Split(" ");

            return m => string.IsNullOrEmpty(value)
                || m.Name.ToLower().Contains(value.ToLower())
                || m.Price.ToString().ToLower().Contains(value.ToLower())
                || m.ManufacturerCountry.ToLower().Contains(value.ToLower())
                || m.ManufacturerEmail.ToLower().Contains(value.ToLower())
                || m.ManufacturerName.ToLower().Contains(value.ToLower());
        }
    }
}
