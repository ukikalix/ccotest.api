﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Dto
{
    public class GetUserRequest
    {
        public string Email { get; set; }
    }

    public class SignInRequest : GetUserRequest
    {
        public string Password { get; set; }
    }

    public class SignUpRequest : GetUserRequest
    {
        public string Password { get; set; }
        public string Name { get; set; }
        public bool SignIn { get; set; }
    }
}
