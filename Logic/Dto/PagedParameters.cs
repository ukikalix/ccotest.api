﻿namespace Logic.Dto
{
    public class PagedParameters
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public bool Ascending { get; set; }
        public string OrderProperty { get; set; }
        public string Search { get; set; }
    }
}
