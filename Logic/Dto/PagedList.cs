﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Dto
{
    public class PagedList<T>
    {
        /// <summary>
        /// Results of Query
        /// </summary>
        public IEnumerable<T> Results { get; set; }
        /// <summary>
        /// Count of results
        /// </summary>
        public int RowCount { get; set; }
        /// <summary>
        /// Size of Page
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Current page 
        /// </summary>
        public int CurrentPage { get; set; }
    }
}
