﻿using Microsoft.EntityFrameworkCore;
using Models.Generic;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositories.Repositories.Base
{
    public abstract class BaseRepository<TEntity, TContext> : IBaseRepository<TEntity>
        where TEntity : class, IModel
        where TContext : DbContext
    {

        private readonly TContext context;

        public BaseRepository(TContext context)
        {
            this.context = context;
        }

        public virtual TEntity Add(TEntity entity)
        {
            GetDbSet().Add(entity);
            return entity;
        }

        public virtual void Delete(TEntity entity)
        {
            GetDbSet().Attach(entity);
            GetDbSet().Remove(entity);
        }

        public virtual void Edit(TEntity entity) => context.Entry(entity).State = EntityState.Modified;

        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate) => GetDbSet().Where(predicate);

        public async Task<TEntity> FindByKey(params object[] keyValues) => await GetDbSet().FindAsync(keyValues);

        public async Task<bool> Any(Expression<Func<TEntity, bool>> predicate) => await GetDbSet().AnyAsync(predicate);

        public IQueryable<TEntity> GetAll() => GetDbSet();

        public DbSet<TEntity> GetDbSet() => context.Set<TEntity>();

        public virtual async Task Save() => await context.SaveChangesAsync();
    }
}
