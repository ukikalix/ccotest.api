﻿using Models.Context;
using Models.Generic;

namespace Repositories.Repositories.Base
{
    public class GenericRepository<TEntity> : BaseRepository<TEntity, AppDbContext>, IGenericRepository<TEntity>
        where TEntity : class, IModel
    {
        public GenericRepository(AppDbContext context) : base(context)
        {
        }
    }
}
